pipeline {
  agent {
    label "jenkins-python"
  }
  environment {
    ORG = 'thiagolinux'
    APP_NAME = 'python-flask-docker'
    CHARTMUSEUM_CREDS = credentials('jenkins-x-chartmuseum')
    DOCKER_REGISTRY_ORG = 'thiagolinux'
    CHART_REPO = 'http://jenkins-x-chartmuseum:8080'
    DEV_ENV_NAME = '${APP_NAME}-dev'
	HOM_ENV_NAME = '${APP_NAME}-hom'
	PROD_ENV_NAME = '${APP_NAME}-prod'
  }
  stages {
      //DESENVOLVIMENTO #################################################################################
    stage('Construcao dos artefatos de Desenvolvimento - CI') {
      when {
        branch 'develop'
      }
      steps {
        container('python') {
          // Carrega credenciais do git
          sh "git config --global credential.helper store"
          sh "jx step git credentials"
          // Gera o numero da nova versao baseado no pom.xml e na ultima tag gerada no GIT
          sh "echo \$(jx-release-version)-alpha > VERSION"
          // Faz o push da nova tag para o git
          sh "jx step tag --version \$(cat VERSION)"
   
          // (OPCIONAL) Gerar BRANCH automaticamente, baseada na TAG recém criada
          // para facilitar Merge de versões pontuais
          //sh "git fetch --all --tags"
          //sh "git branch v\$(cat VERSION) refs/tags/v\$(cat VERSION)"
          //sh "git tag -d v\$(cat VERSION)"
          //sh "git push -u origin v\$(cat VERSION)"
          // ###### FIM DA GERACAO DA BRANCH BASEADA NA TAG #####################

		  //-------------------------------------------------------------------------------------------------------------------
          // Sessao dedicada a construcao da aplicacao e prepracao para criacao de imagem Docker
		  //-------------------------------------------------------------------------------------------------------------------

          // Cria imagem Docker com Skaffold
          sh "export VERSION=`cat VERSION` && skaffold build -f skaffold.yaml"
        }
      }
    }
    stage('Promove para ambiente de Desenvolvimento') {
      when {
        branch 'develop'
      }
      steps {
        container('python') {
          dir("charts/${APP_NAME}") {
            // Gera changelog
            sh "jx step changelog --version \$(cat ../../VERSION)"
            // Versiona e armazena o Helm Chart no repositorio de Charts (Chartmuseum)
            sh "jx step helm release"
            // Faz a promocao para o ambiente
            sh "echo \"jx promote -b --no-poll=true --no-wait=true --env ${DEV_ENV_NAME} --timeout 1h --version \$(cat ../../VERSION)\""
          }
        }
      }
    }
//HOMOLOGACAO #####################################################################################
    stage('Construcao dos artefatos de Homologacao - CI') {
      when {
        branch 'homolog'
      }
      steps {
        container('python') {
          // Carrega credenciais do git
          sh "git config --global credential.helper store"
          sh "jx step git credentials"
          // Faz o fetch das tags
          sh "git fetch --tags"
          // TRATRAMENTO DE HOTFIX #######################################################################
          // Para hotfix direto nesse ambiente, sem passar pelo início da esteira DEV -> HOMOLOG
          // Em situações como por exemplo:
          // HOMOLOG está sendo avalidada pelo cliente com uma massa de dados/estrutura que enquanto avaliação,
          // não pode ser sobrescrita pelo dados vindos de DEV.
          // 1 - Crie uma nova branch com o no padrao: TAGcomBug-hotfix[1..99] Ex: ( v0.0.13-beta-hotfix1 ) baseada na TAG beta com o bug identificado.
          // 2 - Realize as alterações necessárias para correção
          // 3 - Faça um MR (Merge Request) da branch com as correções para essa branch
          // 4 - Atualize correção com a branch develop o quanto antes, para a correção contemplar as novas versões.


          // TRATAMENTO DE TAG ##########################################################################
          // Pega o name da branch de origem do Merge Request
          sh "git log --merges -n 1 | grep \"Merge branch\" | sed -r -n \"s@[ \\t]+Merge branch \'(.+)\' into \'.+\'@\\1@gp\" > FROM-ORIGIN-MERGE"
      
          // Valida se o MR veio da branch develop ou se de outra branch, por exemplo: v0.0.20-alpha
          sh'''#! /bin/bash
                       if grep -e "^develop" FROM-ORIGIN-MERGE -q ; then
                          # Pega a última TAG publicada com a string 'alpha' 
                          echo $(git tag -l --sort=-creatordate | grep -E "alpha" | head -1) > VERSION
                       else
                          # O número da versão a ser construída/promovida será baseada no nome da branch de origem do Merge Request 
                          cat FROM-ORIGIN-MERGE > VERSION
          fi'''

          // Faz o tratamento da tag, alterando a versão de 'alpha' para 'beta'
          sh "sed -i 's|alpha|beta|g;s|v||g' VERSION"        

          // Faz o push da nova tag para o git 
          sh "jx step tag --version \$(cat VERSION)"

          // Cria imagem Docker com Skaffold (com a nova tag)
          sh "export VERSION=`cat VERSION` && skaffold build -f skaffold.yaml"
        }  
      }
    }
    stage('Promove para ambiente de Homologacao') {
      when {
        branch 'homolog'
      }
      steps {
        container('python') {
          dir("charts/${APP_NAME}") {
            // Gera changelog
            sh "jx step changelog --version \$(cat ../../VERSION)"
            // Versiona e armazena o Helm Chart no repositorio de Charts (Chartmuseum)
            sh "jx step helm release"
            // Faz a promocao para o ambiente
            sh "echo \"jx promote -b --no-poll=true --no-wait=true --env ${HOM_ENV_NAME} --timeout 1h --version \$(cat ../../VERSION)\""
          }
        }
      }
    }      
//PRODUCAO ########################################################################################
    stage('Construcao dos artefatos de Producao - CI') {
      when {
        branch 'master'
      }
      steps {
        container('python') {
          // Carrega credenciais do git
          sh "git config --global credential.helper store"
          sh "jx step git credentials"
          // Faz o fetch das tags
          sh "git fetch --tags"

          // TRATRAMENTO DE HOTFIX #######################################################################
          // Para hotfix direto nesse ambiente, sem passar pelo início da esteira DEV -> HOMOLOG -> PROD
          // Em situações como por exemplo:
          // PROD ainda não ápta para receber novas funcionalidades ainda sendo avalidadas em HOMOLOG.
          // 1 - Crie uma nova branch com o no padrao: TAGcomBug-hotfix[1..99] Ex: ( v0.0.13-stable-hotfix1 ) baseada na TAG beta com o bug identificado.
          // 2 - Realize as alterações necessárias para correção
          // 3 - Faça um MR (Merge Request) da branch com as correções para essa branch
          // 4 - Atualize correção com a branch develop o quanto antes, para a correção contemplar as novas versões.


          // TRATAMENTO DE TAG ##########################################################################
          // Pega o name da branch de origem do Merge Request
          sh "git log --merges -n 1 | grep \"Merge branch\" | sed -r -n \"s@[ \\t]+Merge branch \'(.+)\' into \'.+\'@\\1@gp\" > FROM-ORIGIN-MERGE"
      
          // Valida se o MR veio da branch homolog ou se de outra branch, por exemplo: v0.0.20-beta
          sh'''#! /bin/bash
                       if grep -e "^homolog" FROM-ORIGIN-MERGE -q ; then
                          # Pega a última TAG publicada com a string 'beta' 
                          echo $(git tag -l --sort=-creatordate | grep -E "beta" | head -1) > VERSION
                       else
                          # O número da versão a ser construída/promovida será baseada no nome da branch de origem do Merge Request 
                          cat FROM-ORIGIN-MERGE > VERSION
          fi'''

          // Faz o tratamento da tag, alterando a versão de 'beta' para 'stable'
          sh "sed -i 's|beta|stable|g;s|v||g' VERSION"        

		  //-------------------------------------------------------------------------------------------------------------------
          // Sessao dedicada a construcao da aplicacao e prepracao para criacao de imagem Docker

          // Faz o push da nova tag para o git 
          sh "jx step tag --version \$(cat VERSION)"

          // Cria imagem Docker com Skaffold (com a nova tag)
          sh "export VERSION=`cat VERSION` && skaffold build -f skaffold.yaml"
        }  
      }
    }
    stage('Promove para ambiente de Producao') {
      when {
        branch 'master'
      }
      steps {
        container('python') {
          dir("charts/${APP_NAME}") {
            // Gera changelog
            sh "jx step changelog --version \$(cat ../../VERSION)"
            // Versiona e armazena o Helm Chart no repositorio de Charts (Chartmuseum)
            sh "jx step helm release"
            // Faz a promocao para o ambiente
            sh "echo \"jx promote -b --no-poll=true --no-wait=true --env ${PROD_ENV_NAME} --timeout 1h --version \$(cat ../../VERSION)\""
          }
        }
      }
    }      
  }
  post {
        always {
          cleanWs()
        }
  }
}
